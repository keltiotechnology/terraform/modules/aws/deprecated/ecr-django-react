# Shared Input between modules
aws_profile = "default"

region = "eu-central-1"
namespace = "ecs"


# Input for Module VPC

availability_zones = ["eu-central-1a", "eu-central-1b"]
vpc_cidr_block = "172.16.0.0/16"
vpc_name = "vpc"
application_balancer_name = "mycluster-alb"

# Input for Module ECR and ECS
ecs_cluster_name = "my_cluster"
stage     = "test"
name      = "ecr"

image_id = "ami-0b440d17bfb7989dc"
instance_type = "t2.micro"

autoscaling_group_name = "ecs-autoscaling-group"
min_size = 0
max_size = 2
desired_capacity = 2

maximum_scaling_step_size= 2
minimum_scaling_step_size = 1
target_capacity = 2

# Ensure placing the the sample order for docker file paths and image names
docker_files = ["/home/phatvo/WorkAtTekos/ecr-proj/django-ecs", 
                "/home/phatvo/WorkAtTekos/ecr-proj/react-sample-app"]

image_names  = ["hello-world-django-app", 
                "react-app"]


service_names = ["django-as-service", "react-as-service"]

# Input for Module Task definition
backend_app = {
    name = "hello-world-django-app",
    container_name =  "django_container",
    
    host_port = "80",
    container_port = "80",

    container_cpu = 256
    container_memory = 512

    reserved_cpu = 256
    reserved_memory = 512

    container_command = ["gunicorn", "-w", "3", "-b", ":80", "django_sample_project.wsgi:application"]

    desired_count = 1
}


frontend_app = {
    name = "react-app"
    container_name =  "react_container"

    host_port = "8001"
    container_port = "80"

    container_cpu = 256
    container_memory = 471
    
    reserved_cpu = 256
    reserved_memory = 471

    container_command = []

    desired_count = 1
}

# ServiceList = [frontend_app]

backend_heathcheck_path = "/" #"/backend/admin/"

frontend_heathcheck_path = "/"

hosted_zone_id = "Z02042992EWARW63784GW"
domain_name = "conessione.xyz"