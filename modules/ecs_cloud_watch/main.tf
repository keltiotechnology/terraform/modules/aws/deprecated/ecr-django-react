
/*
 * Cloudwatch events
*/

resource "aws_cloudwatch_event_rule" "default" {
  name              = var.cloudwatch_event_rule_name
  description       = var.cloudwatch_event_rule_description

  event_pattern = jsonencode({
    "source"        : ["aws.ecs"],
    "detail-type"   : ["ECS Task State Change", "ECS Container Instance State Change"],
    "detail" : {
      "clusterArn" : [
        "${var.cluster_arn}"
      ]
    }
  })
}

resource "aws_cloudwatch_event_target" "sns" {
  rule              = aws_cloudwatch_event_rule.default.name
  target_id         = "SendToSNS"
  arn               = aws_sns_topic.default.arn
}

resource "aws_sns_topic" "default" {
  name              = "${var.cluster_name}-ecs-topic"
}

resource "aws_sns_topic_policy" "default" {
  arn               = aws_sns_topic.default.arn
  policy            = data.aws_iam_policy_document.sns_topic_policy.json
}

data "aws_iam_policy_document" "sns_topic_policy" {
  statement {
    effect          = "Allow"
    actions         = ["SNS:Publish"]

    principals {
      type          = "Service"
      identifiers   = ["events.amazonaws.com"]
    }

    resources       = [aws_sns_topic.default.arn]
  }
}

resource "aws_sns_topic_subscription" "user_notify" {
  for_each          = toset(var.user_emails)

  topic_arn         = aws_sns_topic.default.arn
  protocol          = "email"
  endpoint          = each.key
}
