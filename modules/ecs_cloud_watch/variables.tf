variable "cluster_arn" {
  type          = string
  description   = "ARN of cluster"
}

variable "cluster_name" {
  type          = string
  description   = "Name of cluster"
}

variable "cloudwatch_event_rule_name" {
  type          = string
  description   = "Name of the Cloudwatch event rule"
}

variable "cloudwatch_event_rule_description" {
  type          = string
  description   = "Description of the Cloudwatch event rule"
  default       = ""
}

variable "user_emails" {
  type          = list(string)
  description   = "List of emails will receive event notification"
}