/*
 * Read Terraform document for more configuration for ECS cluster resource at here:
 * - https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_cluster
*/
resource "aws_ecs_cluster" "default" {
  name = var.cluster_name
  setting {
    name  = "containerInsights"
    value = "enabled"
  }

  capacity_providers = [var.capacity_provider_name]
}


resource "null_resource" "scale_down_asg" {
  # https://discuss.hashicorp.com/t/how-to-rewrite-null-resource-with-local-exec-provisioner-when-destroy-to-prepare-for-deprecation-after-0-12-8/4580/2
  triggers = {
    asg_name = "autoscaling_group_name=${var.autoscaling_group_name},region=${var.region}"
  }

  # Only run during destroy, do nothing for apply.
  provisioner "local-exec" {
    when    = destroy
    working_dir = path.module
    command = <<-EOT
    python script/turn_off_protection.py "${self.triggers.asg_name}"
    EOT

  }
  depends_on = [aws_ecs_cluster.default]
}
