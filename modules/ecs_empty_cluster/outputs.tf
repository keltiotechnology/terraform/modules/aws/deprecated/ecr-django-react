output "cluster_id" {
  value = aws_ecs_cluster.default.id
}

output "cluster_arn" {
  value = aws_ecs_cluster.default.arn
}

output "cluster_tags_all" {
  value = aws_ecs_cluster.default.tags_all
}