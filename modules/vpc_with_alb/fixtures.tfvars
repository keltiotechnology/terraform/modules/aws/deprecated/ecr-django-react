# Sample input definition
region = "eu-central-1"

availability_zones = ["eu-central-1a", "eu-central-1b"]

vpc_cidr_block = "172.16.0.0/16"

namespace = "ecs"

vpc_name = "vpc"

application_balancer_name = "mycluster-alb"