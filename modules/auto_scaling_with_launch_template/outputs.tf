output "capacity_provider_name" {
  value = aws_ecs_capacity_provider.default.name
  description = "Name of the Capacity provider"
}