/*
 * Docs: https://docs.aws.amazon.com/AmazonECS/latest/developerguide/instance_IAM_role.html
*/

/*
 * IAM role and policies
*/



resource "aws_iam_role" "ecs_service_role" {
  name = var.iam_role_name
  assume_role_policy = jsonencode({
    "Version" : "2008-10-17",
    "Statement" : [
      {
        "Action" : "sts:AssumeRole",
        "Principal" : {
          "Service" : [
            "ecs.amazonaws.com",
            "ec2.amazonaws.com"
          ]
        },
        "Effect" : "Allow"
      }
    ]
  })

  provisioner "local-exec" {
    when    = create
    working_dir = path.module
    command = <<-EOT
    echo Starting creating empty cluster for service role
    python script/create_AWSServiceRoleForECS.py
    echo Complete creating empty cluster for service role
    EOT
  }    
}

resource "aws_iam_instance_profile" "default" {
  name = "${var.iam_role_name}_profile"
  path = "/"
  role = aws_iam_role.ecs_service_role.name
}

resource "aws_iam_role_policy_attachment" "ecs_instance_role" {
  role       = aws_iam_role.ecs_service_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}


resource "aws_iam_role_policy" "ecs_service_role-policy" {
  name = "${var.iam_role_name}_ecs_service_role_policy"
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "elasticloadbalancing:Describe*",
          "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
          "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
          "ec2:Describe*",
          "ec2:AuthorizeSecurityGroupIngress",
          "elasticloadbalancing:RegisterTargets",
          "elasticloadbalancing:DeregisterTargets"
        ],
        "Resource" : [
          "*"
        ]
      }
    ]
    }
  )
  role = aws_iam_role.ecs_service_role.id
}
